﻿using IgnitisTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IgnitisTask.Interfaces
{
    public interface IRegistrationService
    {
        Task<IEnumerable<RegistrationDTO>> GetRegistrationsAsync();
        Task<RegistrationDTO> GetRegistrationAttributesByRegistrationIdAsync(int registrationId);
        Task SaveRegistrationOptionsAsync(RegistrationDTO registrationDto);
    }
}
