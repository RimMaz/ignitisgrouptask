﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IgnitisTask.Models;
using IgnitisTask.Interfaces;

namespace IgnitisTask.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRegistrationService _registrationService;

        public HomeController(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }

        [Route("")]
        [Route("RegPoz")]
        [HttpGet]
        public IActionResult Index()
        {
            var registrations = _registrationService.GetRegistrationsAsync().Result;
            return View(registrations);
        }

        [Route("RegPoz/{id}")]
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var registationAttributes = await _registrationService.GetRegistrationAttributesByRegistrationIdAsync(id);
            return View(registationAttributes);
        }
        [Route("RegPoz")]
        [HttpPost]
        public async Task<IActionResult> SaveChanges(RegistrationDTO registrationDto)
        {
            try
            {
                await _registrationService.SaveRegistrationOptionsAsync(registrationDto);
            }
            catch (ArgumentNullException e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();                
            }
            return RedirectToAction("Details", new { id = registrationDto.Id });
        }

        [Route("RegPoz/error")]
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
