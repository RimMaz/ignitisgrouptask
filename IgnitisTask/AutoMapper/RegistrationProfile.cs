﻿using AutoMapper;
using IgnitisTask.DataAccess.Entities;
using IgnitisTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attribute = IgnitisTask.DataAccess.Entities.Attribute;

namespace IgnitisTask.AutoMapper
{
    public class RegistrationProfile : Profile
    {
        public RegistrationProfile()
        {
            CreateMap<Registration, RegistrationDTO>();
            CreateMap<Attribute, AttributeDTO>();
            CreateMap<Option, OptionDTO>();
        }
    }
}
