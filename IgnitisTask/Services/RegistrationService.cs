﻿using AutoMapper;
using IgnitisTask.DataAccess;
using IgnitisTask.DataAccess.Context;
using IgnitisTask.Interfaces;
using IgnitisTask.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IgnitisTask.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly RegistrationContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public RegistrationService(RegistrationContext context, IMapper mapper, ILoggerFactory loggerFactory)
        {
            _context = context;
            _mapper = mapper;
            _logger = loggerFactory.CreateLogger(typeof(RegistrationService));
        }

        public async Task<IEnumerable<RegistrationDTO>> GetRegistrationsAsync()
        {
            return await _context.Registration.Select(o => _mapper.Map<RegistrationDTO>(o)).ToListAsync();
        }

        public async Task SaveRegistrationOptionsAsync(RegistrationDTO registrationDto)
        {
            await SaveOptionChanges(registrationDto);
        }

        public async Task<RegistrationDTO> GetRegistrationAttributesByRegistrationIdAsync(int registrationId)
        {
            var registrationAttributes = await _context.RegistrationAttribute
                 .Where(o => o.RegistrationId == registrationId)
                 .Include(o => o.Attribute.OptionSet.Options)
                 .Include(o => o.Registration)
                 .ToListAsync();

            if (!registrationAttributes.Any())
            {
                var message = "Get attributes for registration:" + registrationId + ", returned no attributes.";            
                _logger.LogWarning(message);
                return new RegistrationDTO() { Id = registrationId, Attributes = new List<AttributeDTO>() };
            }

            try
            {
                var registrationResult = _mapper.Map<RegistrationDTO>(registrationAttributes.FirstOrDefault().Registration);
                registrationResult.Attributes = new List<AttributeDTO>();

                foreach (var regAttribute in registrationAttributes)
                {
                    var attributeResult = _mapper.Map<AttributeDTO>(regAttribute.Attribute);
                    attributeResult.Options = new List<OptionDTO>();

                    foreach (var option in regAttribute.Attribute.OptionSet.Options)
                    {
                        var optionResult = _mapper.Map<OptionDTO>(option);

                        attributeResult.Options.Add(optionResult);
                    }
                    registrationResult.Attributes.Add(attributeResult);
                }
                return registrationResult;
            }
            catch (Exception e)
            {
                var message = "Get attributes for registration:" + registrationId + ", failed at returning options.";
                _logger.LogError(e.ToString());
                return new RegistrationDTO() { Id = registrationId, Attributes = new List<AttributeDTO>() };
            }
        }

        private async Task SaveOptionChanges(RegistrationDTO registrationDto)
        {
            if (registrationDto == null || registrationDto.Attributes == null)
            {
                var message = "While saving registration, argument is null or doesn't contain attributes parameter passed: registrationDto";
                _logger.LogError(message);
                throw new ArgumentNullException(message);
            }

            foreach(var attribute in registrationDto.Attributes)
            {
                var attributeEntity = await _context.Attribute.FirstOrDefaultAsync(o => o.Id == attribute.Id);
                attributeEntity.SelectedOptionId = attribute.SelectedOptionId;

                await _context.SaveChangesAsync();
            }
        }       
    }
}
