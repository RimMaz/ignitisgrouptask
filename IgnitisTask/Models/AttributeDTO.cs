﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IgnitisTask.Models
{
    public class AttributeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDropdown { get; set; }
        public bool IsBoolean { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsString { get; set; }
        public bool IsSelected { get; set; }
        public int SelectedOptionId { get; set; }
        public List<OptionDTO> Options { get; set; }
    }
}
