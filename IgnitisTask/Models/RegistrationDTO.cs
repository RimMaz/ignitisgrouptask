﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IgnitisTask.Models
{
    public class RegistrationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<AttributeDTO> Attributes { get; set; }
    }
}
