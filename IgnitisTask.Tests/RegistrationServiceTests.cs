using AutoMapper;
using IgnitisTask.DataAccess.Context;
using IgnitisTask.Interfaces;
using IgnitisTask.Models;
using IgnitisTask.Services;
using IgnitisTask.Tests.Mocks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IgnitisTask.Tests
{
    [TestClass]
    public class RegistrationServiceTests : IDisposable
    {
        private readonly RegistrationContext _context;
        private readonly IRegistrationService _service;
        private readonly IMapper _mapper;
        private readonly NullLoggerFactory _loggerFactory = new NullLoggerFactory();

        public RegistrationServiceTests()
        {
            _context = new RegistrationContextMock(new DbContextOptionsBuilder<RegistrationContext>());
            _mapper = new AutoMapperBuilder().BuildMapper();
            _service = new RegistrationService(_context, _mapper, _loggerFactory);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public void GetRegistrationAttributesByRegistrationIdAsync_ReturnsList_SameCountAndCorrectType()
        {
            //Arrange
            var firstRegistrationId = _context.Registration.FirstOrDefault().Id;
            var expectedAttributes = _context.Attribute.ToListAsync().Result;

            //Act
            var actual = _service.GetRegistrationAttributesByRegistrationIdAsync(firstRegistrationId).Result;

            //Assert
            Assert.AreEqual(expectedAttributes.Count, actual.Attributes.Count);
            Assert.IsInstanceOfType(actual, typeof(RegistrationDTO));
        }

        [TestMethod]
        public void GetRegistrationsAsync_ReturnsList_SameCountAndCorrectType()
        {
            //Arrange
            var expected = _context.Registration.ToListAsync().Result;

            //Act
            var actual = _service.GetRegistrationsAsync().Result.ToList();

            //Assert
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.IsInstanceOfType(actual, typeof(IEnumerable<RegistrationDTO>));
        }       

        [TestMethod]
        public void SaveRegistrationAsync_SameCountAndCorrectType()
        {
            //Arrange
            var firstRegistrationId = _context.Registration.FirstOrDefault().Id;
            var registrationAttributes = _context.RegistrationAttribute
               .Where(o => o.RegistrationId == firstRegistrationId)
               .Include(o => o.Attribute.OptionSet.Options)
               .Include(o => o.Registration)
               .ToList();

            var attribute1 = registrationAttributes.FirstOrDefault().Attribute;
            attribute1.SelectedOptionId = 1;
            var attribute2 = registrationAttributes.LastOrDefault().Attribute;
            attribute2.SelectedOptionId = 3;
            _context.SaveChanges();

            var registrationDto = new RegistrationDTO
            {
                Id = firstRegistrationId,
                Name = "Test",
                Attributes = new List<AttributeDTO>
                {
                    new AttributeDTO(){Id = attribute1.Id, SelectedOptionId = 2},
                    new AttributeDTO(){Id = attribute2.Id, SelectedOptionId = 4}
                }
            };

            //Act
            var actual = _service.SaveRegistrationOptionsAsync(registrationDto);

            attribute1 = registrationAttributes.FirstOrDefault().Attribute;
            var attriubute1ActualSelectionId = attribute1.SelectedOptionId;
            attribute2 = registrationAttributes.LastOrDefault().Attribute;
            var attriubute2ActualSelectionId = attribute2.SelectedOptionId;

            //Assert
            Assert.AreEqual(2, attriubute1ActualSelectionId);
            Assert.AreEqual(4, attriubute2ActualSelectionId);
        }
    }
}
