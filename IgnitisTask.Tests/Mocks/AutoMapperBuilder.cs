﻿using AutoMapper;
using IgnitisTask.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace IgnitisTask.Tests.Mocks
{
    public class AutoMapperBuilder
    {
        private readonly MapperConfiguration _mappingConfiguration;
        public AutoMapperBuilder()
        {
            _mappingConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new RegistrationProfile());
            });
        }

        public IMapper BuildMapper()
        {
            return _mappingConfiguration.CreateMapper();
        }
    }
}
