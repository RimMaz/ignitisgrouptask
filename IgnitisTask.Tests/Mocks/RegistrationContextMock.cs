﻿using IgnitisTask.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace IgnitisTask.Tests.Mocks
{
    public class RegistrationContextMock : RegistrationContext
    {
        public RegistrationContextMock(DbContextOptionsBuilder<RegistrationContext> builder)
     : base(builder.UseInMemoryDatabase(Guid.NewGuid().ToString())
         .EnableSensitiveDataLogging()
         .ConfigureWarnings(a => a.Ignore(InMemoryEventId.TransactionIgnoredWarning))
         .Options)
        {
            FillModel();
        }

        public void FillModel()
        {
            DbInitializer.Initialize(this);
        }
    }
}
