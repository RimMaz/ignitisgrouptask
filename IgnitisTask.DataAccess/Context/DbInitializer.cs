﻿using IgnitisTask.DataAccess.Context;
using IgnitisTask.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Attribute = IgnitisTask.DataAccess.Entities.Attribute;

namespace IgnitisTask.DataAccess.Context
{
    public static class DbInitializer
    {
        public static void Initialize(RegistrationContext context)
        {
            context.Database.EnsureCreated();

            if (context.Registration.Any())
            {
                return;   // DB has been seeded
            }

            var registations = new Registration[]
            {
                new Registration(){ Name = "reg1"}
            };

            var attributes = new Attribute[]
            {
                new Attribute(){ Name = "attribute1", IsBoolean = true, IsDropdown = true, IsString = false, IsNumeric = false, IsSelected = false, SelectedOptionId = 1},
                new Attribute(){ Name = "attribute2", IsBoolean = false, IsDropdown = true, IsString = true, IsNumeric = false, IsSelected = false, SelectedOptionId = 3},
            };

            var optionSets = new OptionSet[]
            {
                new OptionSet(){ Name = "optionSet1"},
                new OptionSet(){ Name = "optionSet2"}
            };

            var boolOptions = new Option[]
            {
                new Option(){ Name = "option1", Value = "True"},
                new Option(){ Name = "option2", Value = "False"}               
            };

            var stringOptions = new Option[]
            {
                new Option(){ Name = "option3", Value = "test1", },
                new Option(){ Name = "option4", Value = "test2"}
            };

            foreach (var registration in registations)
            {
                context.Registration.Add(registration);
            }

            foreach (var set in optionSets)
            {
                context.OptionSet.Add(set);
            }

            context.SaveChanges();

            foreach (var option in boolOptions)
            {
                option.OptionSetId = optionSets[0].Id;
                context.Option.Add(option);
            }

            foreach (var option in stringOptions)
            {
                option.OptionSetId = optionSets[1].Id;
                context.Option.Add(option);
            }

            context.SaveChanges();

            attributes[0].OptionSetId = optionSets[0].Id;
            attributes[1].OptionSetId = optionSets[1].Id;

            context.Attribute.AddRange(attributes);

            context.SaveChanges();

            var registrationAttribute1 = new RegistrationAttribute
            {
                RegistrationId = registations[0].Id,
                AttributeId = attributes[0].Id
            };

            context.RegistrationAttribute.Add(registrationAttribute1);

            var registrationAttribute2 = new RegistrationAttribute
            {
                RegistrationId = registations[0].Id,
                AttributeId = attributes[1].Id
            };

            context.RegistrationAttribute.Add(registrationAttribute2);

            context.SaveChanges();
        }
    }
}
