﻿using IgnitisTask.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = IgnitisTask.DataAccess.Entities.Attribute;

namespace IgnitisTask.DataAccess.Context
{
    public class RegistrationContext : DbContext
    {
        public RegistrationContext(DbContextOptions<RegistrationContext> options)
         : base(options)
        { }

        public virtual DbSet<Registration> Registration { get; set; }
        public virtual DbSet<RegistrationAttribute> RegistrationAttribute { get; set; }
        public virtual DbSet<Attribute> Attribute { get; set; }
        public virtual DbSet<OptionSet> OptionSet { get; set; }
        public virtual DbSet<Option> Option { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RegistrationAttribute>().HasKey(x => new { x.RegistrationId, x.AttributeId });
        }
    }
}
