﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IgnitisTask.DataAccess.Entities
{
    public class RegistrationAttribute
    {
        [ForeignKey("Registration")]
        public int RegistrationId { get; set; }

        [ForeignKey("Attribute")]
        public int AttributeId { get; set; }

        public virtual Registration Registration { get; set; }
        public virtual Attribute Attribute { get; set; }
    }
}
