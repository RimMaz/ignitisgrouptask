﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IgnitisTask.DataAccess.Entities
{
    public class Option
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        [ForeignKey("OptionSet")]
        public int OptionSetId { get; set; }

        public virtual OptionSet OptionSet { get; set; }
    }
}
