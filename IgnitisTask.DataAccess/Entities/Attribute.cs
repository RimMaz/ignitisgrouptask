﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IgnitisTask.DataAccess.Entities
{
    public class Attribute
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDropdown { get; set; }
        public bool IsBoolean { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsString { get; set; }
        public bool IsSelected { get; set; }
        public int? SelectedOptionId { get; set; }

        [ForeignKey("OptionsSet")]
        public int OptionSetId { get; set; }

        public virtual OptionSet OptionSet { get; set; }
        public virtual ICollection<RegistrationAttribute> RegistrationAttribute { get; set; }
    }
}
