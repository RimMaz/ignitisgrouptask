﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IgnitisTask.DataAccess.Migrations
{
    public partial class addedOptionSaving : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSelected",
                table: "Attribute",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SelectedOptionId",
                table: "Attribute",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSelected",
                table: "Attribute");

            migrationBuilder.DropColumn(
                name: "SelectedOptionId",
                table: "Attribute");
        }
    }
}
